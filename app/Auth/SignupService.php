<?php

namespace App\Auth;

use App\Db\DatabaseException;
use App\Db\DatabaseService;
use App\Api\ApiException;
use App\Api\CurlService;
use mysqli;

/**
 * Class UserService
 * @package App\Auth
 */
class SignupService
{
    const API_URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
    private mysqli $db;
    private CurlService $curl;

    /**
     * @return SignupService
     */
    public static function create()
    {
        return new self(DatabaseService::create()->connection(), CurlService::create());
    }

    /**
     * SignupService constructor.
     * @param mysqli $db
     * @param CurlService $curl
     */
    public function __construct(mysqli $db, CurlService $curl)
    {
        $this->db = $db;
        $this->curl = $curl;
    }

    /**
     * @return mysqli
     */
    public function getDb(): mysqli
    {
        return $this->db;
    }

    /**
     * @param mysqli $db
     */
    public function setDb(mysqli $db): void
    {
        $this->db = $db;
    }

    /**
     * @return CurlService
     */
    public function getCurl(): CurlService
    {
        return $this->curl;
    }

    /**
     * @param CurlService $curl
     */
    public function setCurl(CurlService $curl): void
    {
        $this->curl = $curl;
    }

    /**
     * Registers a user
     * @param array $user_params
     * @return string
     */
    public function register($user_params)
    {
        $user = new User($user_params);
        $this->persist($user);
        $user->paymentDataId = $this->apiEndPoint($user);
        $this->persist($user);

        return $user->paymentDataId;
    }

    /**
     * Persists a user
     * @param User $user
     */
    private function persist(User $user)
    {
        if ($user->account_owner == '' || $user->iban == '') {
            throw new DatabaseException('account_owner or iban cannot be empty');
        }
        $sql = 'INSERT INTO users(id, firstname, lastname, telephone, street, number, zip_code, city, account_owner, iban, paymentDataId)
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE
                    firstname=VALUES(firstname),
                    lastname=VALUES(lastname),
                    telephone=VALUES(telephone),
                    street=VALUES(street),
                    number=VALUES(number),
                    zip_code=VALUES(zip_code),
                    city=VALUES(city),
                    account_owner=VALUES(account_owner),
                    iban=VALUES(iban),
                    paymentDataId=VALUES(paymentDataId);';
        $statement = $this->db->prepare($sql);
        if (!$statement) {
            throw new DatabaseException("Binding '${sql}' failed: " . $this->db->error);
        }
        $statement->bind_param(
            'isssssissss',
            $user->id,
            $user->firstname,
            $user->lastname,
            $user->telephone,
            $user->street,
            $user->number,
            $user->zip_code,
            $user->city,
            $user->account_owner,
            $user->iban,
            $user->paymentDataId
        );
        if (!$statement->execute()) {
            throw new DatabaseException("Query '${sql}' failed: " . $statement->error);
        }
        $user->id = $statement->insert_id;
    }

    /**
     * Call API Endpoint
     *
     * @param User $user
     * @return string
     */
    public function apiEndPoint(User $user)
    {
        $post = [
            'customerId' => $user->id,
            'iban' => $user->iban,
            'owner' => $user->account_owner,
        ];
        $response = $this->curl->post(self::API_URL, $post);
        if (!isset($response['paymentDataId'])) {
            throw new ApiException('invalid API response: ' . var_export($response, true));
        }

        return $response['paymentDataId'];
    }
}
