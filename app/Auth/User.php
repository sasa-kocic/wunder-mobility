<?php

namespace App\Auth;

/**
 * Class User
 * @package App\Auth
 */
class User
{
    public ?int $id;
    public string $firstname;
    public string $lastname;
    public string $telephone;
    public string $street;
    public string $number;
    public int $zip_code;
    public string $city;
    public string $account_owner;
    public string $iban;
    public ?string $paymentDataId;

    public function __construct($data)
    {
        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->telephone = $data['telephone'];
        $this->street = $data['street'];
        $this->number = $data['number'];
        $this->zip_code = (int) $data['zip_code'];
        $this->city = $data['city'];
        $this->account_owner = $data['account_owner'];
        $this->iban = $data['iban'];
        $this->paymentDataId = $data['paymentDataId'] ?? null;
    }
}
