<?php

namespace App\Auth;

use App\Layout\Template;
use RuntimeException;

/**
 * Class SignupController
 * @package App\Auth
 */
class SignupController
{
    private SignupService $service;

    /**
     * @return SignupController
     */
    public static function create()
    {
        $controller = new self();
        $controller->service = SignupService::create();

        return $controller;
    }

    /**
     * Display form
     *
     * @return string
     */
    public function index(): string
    {
        $value = file_get_contents(__DIR__ . "/signup.html");

        return Template::replace('body', $value);
    }

    /**
     * Register a user
     *
     * @param array $post
     * @return string
     */
    public function register($post = []): string
    {
        try {
            $paymentDataId = $this->service->register($post);
        } catch (RuntimeException $e) {
            return Template::view(__DIR__ . '/error.html', ['error' => $e->getMessage()]);
        }

        return Template::view(__DIR__ . '/register.html', ['paymentDataId' => $paymentDataId]);
    }
}
