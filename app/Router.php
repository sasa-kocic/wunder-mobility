<?php

namespace App;

use App\Auth\SignupController;

/**
 * Class Router
 * @package App
 */
class Router
{
    /**
     * @var InterfaceController[]
     */
    public array $controllers = [];

    /**
     * Router constructor.
     * @param InterfaceController[] $controllers
     */
    public function __construct($controllers) {
        $this->controllers = $controllers;
    }

    /**
     * @return Router
     */
    public static function create()
    {
        $controllers = [
            'index' => IndexController::create(),
            'signup' => SignupController::create()
        ];
        return new self($controllers);
    }

    /**
     * Get Request path
     *
     * @param string $request_uri
     * @return string
     */
    public function getPath(string $request_uri)
    {
        $a = explode('?', $request_uri);
        return $a ? $a[0] : $request_uri;
    }

    /**
     * Get Params from Request
     *
     * @param string $request_uri
     * @return array
     */
    public function getParams(string $request_uri)
    {
        $a = explode('?', $request_uri);
        $query_params = isset($a[1]) ? $a[1] : '';
        $params = explode('&', $query_params);
        $output = [];
        for ($i = 0; $i < count($params); $i++) {
            $a = explode('=', $params[$i]);
            $output[$a[0]] = $a[1] ?? '';
        }
        return array_map(function ($s) {
            return filter_var($s, FILTER_SANITIZE_STRING);
        }, $output);
    }

    /**
     * Route the requests
     * @param string $request_uri
     * @return string
     */
    public function route($request_uri): string
    {
        $url_path = $this->getPath($request_uri);
        $url_params = $this->getParams($request_uri);
        switch (true) {
            case preg_match('/^\/signup\/?$/', $url_path):
                $html = $this->controllers['signup']->index();
                break;
            case preg_match('/^\/signup\/register\/?/', $url_path):
                $html = $this->controllers['signup']->register($_POST);
                break;
            case preg_match('/^\/\/?$/', $url_path):
                $html = $this->controllers['index']->index();
                break;
            default:
                $html = $this->controllers['index']->error404($url_path);
                break;
        }
        return $html;
    }
}
