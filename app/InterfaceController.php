<?php

namespace App;

interface InterfaceController {
    public function index(): string;
}