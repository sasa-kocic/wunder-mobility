<?php

namespace App\Api;

use RuntimeException;

/**
 * Class ApiException
 * @package App\Api
 */
class ApiException extends RuntimeException
{
}
