<?php

namespace App\Api;

/**
 * Class CurlService
 * @package App\Api
 */
class CurlService
{
    /**
     * @return CurlService
     */
    public static function create()
    {
        return new self();
    }

    /**
     * @param $url
     * @param array $data
     * @return array
     * @throws CurlException
     */
    public function post($url, $data = [])
    {
        $ch = curl_init();
        $headers = [
            'Content-Type: application/json',
        ];
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        if (!$response) {
            throw new CurlException(curl_error($ch));
        }
        curl_close($ch);

        return json_decode($response, true);
    }
}
