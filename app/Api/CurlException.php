<?php

namespace App\Api;

use RuntimeException;

/**
 * Class CurlException
 * @package App\Api
 */
class CurlException extends RuntimeException
{
}
