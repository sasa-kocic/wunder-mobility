<?php

namespace App\Layout;

class Template
{
    public static function replace($key, $value = '', $html = '')
    {
        if ($html == '') {
            $html = file_get_contents(__DIR__ . '/template.html');
        }

        return str_replace('{{ ' . $key . ' }}', $value, $html);
    }

    public static function view(string $filename, array $vars)
    {
        $html = file_get_contents($filename);
        foreach ($vars as $key => $value) {
            $html = self::replace($key, $value, $html);
        }
        $html = self::replace('body', $html);

        return $html;
    }
}
