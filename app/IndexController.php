<?php

namespace App;

use App\Layout\Template;

class IndexController implements InterfaceController {
    /**
     * @return IndexController
     */
    public static function create()
    {
        return new self();
    }

    public function index(): string
    {
        return Template::replace('body', file_get_contents(__DIR__ . '/index.html'));
    }

    public function error404($url_path): string
    {
        $html = '<div class="container">';
        $html .= '  <h2>Error 404</h2>';
        $html .= "  <p>Unknown path: ${url_path}</p>";
        $html .= '</div>';

        return Template::replace('body', $html);
    }
}
