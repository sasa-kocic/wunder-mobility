<?php

namespace App\Db;

use RuntimeException;

/**
 * Class DatabaseException
 * @package App\Db
 */
class DatabaseException extends RuntimeException
{
}
