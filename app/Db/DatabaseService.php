<?php

namespace App\Db;

use mysqli;

/**
 * Class DatabaseService
 * @package App\Db
 */
class DatabaseService
{
    private ?mysqli $connection = null;

    /**
     * @return DatabaseService
     */
    public static function create()
    {
        return new self();
    }

    /**
     * Database connection
     *
     * @return mysqli|null
     */
    public function connection(): ?mysqli
    {
        if ($this->connection == null) {
            $this->connection = $this->connect();
        }

        return $this->connection;
    }

    /**
     * Connect to Database
     *
     * @return mysqli
     */
    private function connect(): mysqli
    {
        $db = new mysqli('127.0.0.1', 'user', $_ENV['MYSQL_PASSWORD'] ?? 'password', 'wunder-mobility');
        if ($db->connect_error) {
            throw new DatabaseException('Connection to MySQL failed: ' . mysqli_connect_errno());
        }

        return $db;
    }
}
