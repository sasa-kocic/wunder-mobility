CREATE DATABASE `wunder-mobility` IF NOT EXISTS;
USE DATABASE `wunder-mobility`;
CREATE TABLE `users` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `zip_code` int(6) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `account_owner` varchar(255) DEFAULT NULL,
  `iban` varchar(22) DEFAULT NULL,
  `paymentDataId` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;