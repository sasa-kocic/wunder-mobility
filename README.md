# Wunder Mobility

## Test assignment

Build an web application which tackles the everyday problems we face at Wunderfleet. Please create a git repository for this task, before you start working.
The web application should be a basic user registration, like typically known from similar projects. The registration process contains 4 separated steps. Only one step is shown at a time to the customer.

View 1: Insert personal information
- Firstname, lastname, telephone

View 2: Insert address information
- Address including street, house number, zip code, city

View 3: Insert payment information
- Account owner
- IBAN (doesn’t need to be validated)
- When clicking the “next” button, the inserted data should be saved in your custom configured database(mysql/sqlite) and the payment data should be posted to our external “demo-payment” api with the following configuration: URL: https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b ackend-dev-save-payment-data

Example Body of the POST request:
```json
{
    "customerId": 1,
    "iban": "DE8234",
    "owner": "Max Mustermann" 
}
```
Returns 200 OK on success and the following JSON example response: 
```json
{
  "paymentDataId": "c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e7b2a41ed228d86975c9997b96b8bb0da030d34a2be95" 
}
```
Save the “paymentDataId” to the customer dataset in your database.
On errors, we’re returning an error and 4xx Status Code. In the raw Body of the POST request you need to replace the example data with the inserted data from the user.

View 4: Success page
- Success message, when data are successful saved
- Show the “paymentDataId”, returnted by demo endpoint

### Additional Information:

The user is able to leave the registration on every step/view until he finished the whole registration successfully.
Accordingly to this he should be redirected to the last opened step when he’s joining the process again(re-navigating to the webpage). So the state of already inserted data needs to be saved.
There is no need to validate all form inputs, just use the basic html validators if present.You don’t need to implement access control features. Just implement these Steps.
When you are ready and done, please create a remote repository in Bitbucket, push your project into this repository and share it with us.

Please include an export of your Database structure in the project.

Please add a README.txt in your project and answer the following questions in this file:
1. Describe possible performance optimizations for your Code.
2. Which things could be done better, than you’ve done it?

Keep the UI minimal but straightforward, and do not worry about the design, we do not have any restrictions here, native UI elements will suffice! If you feel the need to add some cool features to our small app, feel free to do so. It will be a big plus if you structure your code using the MVP/MVI/MVVM pattern, or any other pattern that you find suitable (and let us know why).

## Init

```bash
composer init
composer require php
composer require ext-mysqlnd
composer require --dev phpunit/phpunit
composer dump-autoload -o
```

### How do I get set up? ###

* Summary of set up
  composer install
* Configuration
* Dependencies
  MySQL database
* Database configuration
  Local database on 127.0.0.1 with user and wunder-mobility database. Password is in ENV MYSQL_PASSWORD or 'password'
* How to run tests
  vendor/bin/phpunit tests
* Deployment instructions
  php -S 0.0.0.0:80 -t public/index.php

### Contribution guidelines ###

* Writing tests
* Code review
* [Code guidelines](https://www.php-fig.org/psr/psr-12/)

### Who do I talk to? ###

* Repo owner or admin
