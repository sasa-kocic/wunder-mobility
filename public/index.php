<?php
    namespace App;

    require_once __DIR__ . '/../vendor/autoload.php';
    echo Router::create()->route($_SERVER['REQUEST_URI']);