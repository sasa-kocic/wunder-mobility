<?php declare(strict_types=1);

use App\Router;
use PHPUnit\Framework\TestCase;

final class RouterTest extends TestCase
{
    public function testCanBeCreated(): void
    {
        $this->assertInstanceOf(
            Router::class,
            Router::create()
        );
    }

    public function testGetPath(): void
    {
        $result = Router::create()->getPath('/signup?var=32&x=3');
        $this->assertEquals('/signup', $result);
    }

    public function testGetParams(): void
    {
        $result = Router::create()->getParams('/signup?var=32&x=3');
        $expected = ['x' => 3, 'var' => 32];
        $this->assertEquals($expected, $result);
    }

    public function testSignupRoute(): void
    {
        $result = Router::create()->route('/signup?var=32&x=3');
        $expected = '<h2>Signup</h2>';
        $this->assertStringContainsString($expected, $result);
    }

    public function testError404(): void
    {
        $indexController = $this->createStub(\App\IndexController::class);
        $indexController->method('error404')->willReturn('<some>Error 404</some>');
        $controllers = [
            'index' => $indexController,
            'signup' => $this->createStub(\App\Auth\SignupController::class)
        ];
        $router = new Router($controllers);
        $result = $router->route('/invalidUrl');
        $expected = '<some>Error 404</some>';
        $this->assertStringContainsString($expected, $result);
    }

    public function testIndexControllerIndexMethod(): void
    {
        $indexController = $this->createStub(\App\IndexController::class);
        $indexController->method('index')->willReturn('<some>Hello</some>');
        $controllers = [
            'index' => $indexController,
            'signup' => $this->createStub(\App\Auth\SignupController::class)
        ];
        $router = new Router($controllers);
        $result = $router->route('/');
        $expected = '<some>Hello</some>';
        $this->assertStringContainsString($expected, $result);
    }

    public function testSignupController(): void
    {
        $signupController = $this->createStub(\App\Auth\SignupController::class);
        $signupController->method('index')->willReturn('<some>Signup</some>');
        $controllers = [
            'index' => $this->createStub(\App\IndexController::class),
            'signup' => $signupController
        ];
        $router = new Router($controllers);
        $result = $router->route('/signup');
        $expected = '<some>Signup</some>';
        $this->assertStringContainsString($expected, $result);

    }

    public function testSignupControllerRegisterMethod(): void
    {
        $signupController = $this->getMockBuilder(\App\Auth\SignupController::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();
        $signupController->method('register')->willReturn('<some>Registration</some>');
        $controllers = [
            'index' => $this->createStub(\App\IndexController::class),
            'signup' => $signupController
        ];
        $router = new Router($controllers);
        $result = $router->route('/signup/register');
        $expected = '<some>Registration</some>';
        $this->assertSame($expected, $result);
    }
}